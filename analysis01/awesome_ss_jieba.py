# encoding='utf-8'

import pandas, jieba, nltk, re, matplotlib

import os, time

import jieba.analyse

import common.Const as CONST

matplotlib.rcParams['font.sans-serif'] = 'SimHei'





'''
#读取原始文件
word = open(CONST.BASE_LIU_FILE_PATH + '\\all.txt', "r", encoding='utf-8').read()

#去除标点
cleaned_data = ''.join(re.findall(r'[\u4e00-\u9fa5]', word))
# print(cleaned_data)
print("1********************************************************")
#去除标点的内容写入文件
open(CONST.BASE_LIU_FILE_PATH + '\\all_clean.txt', "w", encoding='utf-8').write(cleaned_data)
'''
#读取去除标点的文件
#cleaned_data =""
#with open(CONST.BASE_LIU_FILE_PATH + '\\all_clean.txt', "r", encoding='utf-8') as r:
#   cleaned_data = r.read()


#读取停用词(中间用英文','分隔)
sr = open(CONST.BASE_LIU_FILE_PATH + '\\stopwords.txt', "r", encoding='utf-8').read().split(",")



#进行分词
#word_list = jieba._lcut_for_search(cleaned_data)

#分好的词写入文件
#open('data_jieba_over.txt', 'w', encoding='UTF-8').write(' '.join(word_list))


#读取已经分好的词
word_list = open('E:\workspace\py\python-common\png\data_jieba_over.txt', 'r', encoding='UTF-8').read().split(' ')



#去除停用词
world_list_clean = list()
for w in word_list:
    if w not in sr:
        world_list_clean.append(w)




text = nltk.Text(world_list_clean)

#计算词频
freq = nltk.FreqDist(text)
#boff_sort(freq)
freq.plot(20)


#jieba.analyse.TFIDF(idf_path=None)
#tags = jieba.analyse.extract_tags(open('E:\work\刘老师文件\\all_clean.txt','r',encoding='utf-8').read(), topK=20, withWeight=False, allowPOS=())
#print(",".join(tags))

