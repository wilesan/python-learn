# -*- coding:utf-8 -*-
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from lppls import lppls, data_loader

#path = 'C:\Users\ss\Desktop\s2.csv'
df=pd.read_csv(r'C:\Users\ss\Desktop\s2.csv', encoding='utf-8')
#df=pd.read_csv(path, usecols=[5],error_bad_lines=False,warn_bad_lines=False, encoding='ISO-8859-1')
df['new_date'] = df['发布日期'].str[0:-3]
#df['temp_str'] = pd.DataFrame(['0.00']*df.size)
#df['new_date'] = df['new_date']+df['temp_str']
df_sum = df.groupby('new_date')['标题／微博内容'].agg(np.size)
#df['sum'] = df.groupby('new_date')['标题／微博内容'].agg(np.size)
# read example dataset into df
sum = df_sum.tolist()
# convert index col to evenly spaced numbers over a specified interval
#time = df.index.to_list()
time = np.linspace(0, df_sum.size-1, df_sum.size)
# create Mx2 matrix (expected format for LPPLS observations)
observations = np.vstack((time, np.array(df_sum)))
# set the max number for searches to perform before giving-up
# the literature suggests 25
MAX_SEARCHES = 25
# instantiate a new LPPLS model with the S&P 500 dataset
lppls_model = lppls.LPPLS(observations=observations)
# fit the model to the data and get back the params
tc, m, w, a, b, c, c1, c2 = lppls_model.fit(observations, MAX_SEARCHES, minimizer='Nelder-Mead')
# visualize the fit
lppls_model.plot_fit()
