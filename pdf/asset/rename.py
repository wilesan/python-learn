import os
import re
base_dir = "d:/temp"

####读取编号
f = open(base_dir + "/2.txt","r",encoding='gbk',errors='ignore')
f_n1 = []
line = f.readline()
pattern = re.compile('[0-9]+')

while line: #直到读取完文件
    line = f.readline().strip('\n') #去除换行符
    # 匹配字符串中是否含有数字，含有字母的话是[a-z]
    # findall是返回所有符合匹配的项，返回形式为数组
    # if后加变量的意思是判断变量是否为空，不为空则为true，反之为false
    if pattern.findall(line):
        f_n1.append(re.findall("[A-Z\d]+",line)[0])
    else:
        print('no numbers')
f.close
f_n1.insert(0,"20133415")

####读取图片路径
files = os.listdir(base_dir + "/2/")

n,i =0,0
for file in files:
    oldName = file
    newName = f_n1[n] + '.jpg'
    os.rename(base_dir+"/2/"+oldName,base_dir+"/2/"+newName)
    print(oldName, '======>', newName)
    n = n+1
