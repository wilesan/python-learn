import camelot as clt
import pandas as pd
import os

baseUrl = "E:\\PROJECT\\zhouyumo\\data"
targetFile = "E:\\PROJECT\\zhouyumo\\BWM subsidiary product matrix.xlsx"

#获取文件列表
def getFileList():
    print("开始读取",baseUrl,"下的文件列表")
    file_names = os.listdir(baseUrl)
    file_list = [os.path.join(baseUrl,file) for file in file_names]
    print("文件列表获取完成：",file_list)
    print("共",len(file_list),"个文件！")
    return file_list

#解析表格中的数据
def parseContent(file,bank='工银'):
    tables = readPdf(file)
    my_table = pd.DataFrame(columns=('title', 'content'))
    for tb in tables._tables:
        temp_table = None
        if tb.shape[1] > 7:
            print(tb.df)
        elif tb.shape[1] ==7:
            print("文件有5列提取第1列和第3列")
            temp_table = pd.DataFrame(tb.df)[[1, 5]].rename(columns={1: 'title', 5: 'content'})
            print("文件提取完成，并转换成Dataframe")
        elif tb.shape[1] == 5:
            print("文件有5列提取第1列和第3列")
            temp_table = pd.DataFrame(tb.df)[[1,3]].rename(columns={1:'title',3:'content'})
            print("文件提取完成，并转换成Dataframe")
        else:
            temp_table = pd.DataFrame(tb.df).rename(columns={0:'title',1:'content'})
            print("转换成Dataframe")
        if isinstance(temp_table,pd.DataFrame):
            for index,row in temp_table.iterrows():
                if row['title']!="":
                    my_table = my_table.append({'title':row['title'].replace('\n',''), 'content':row['content'].replace('\n','')},ignore_index=True)
            print("第一个表添加完成\n------------------------------")
            print(my_table)

    print(my_table)
    my_table.to_csv(file.replace('pdf','csv'),encoding='gbk')



#读取pdf中的表格数据
def readPdf(path):
    print("开始读取pdf:",path)
    tables = clt.read_pdf(path, flavor='lattice', pages='all',line_scale=40)
    print("读取pdf:",path,"完成")
    return tables



if __name__ == '__main__':
    fileList = getFileList()
    for file in fileList:
        parseContent(file)
    #parseContent(fileList[0])
