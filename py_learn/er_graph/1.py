import networkx as nx
import matplotlib.pyplot as plt
import math

ER = nx.random_graphs.erdos_renyi_graph(10000,0.01)
pos = nx.spring_layout(ER)
nx.draw(ER,pos,with_labels=False,node_size = 20)
plt.show()