'''
10、请编写一个函数实现将IP地址转换成一个整数。

如 10.3.9.12 转换规则为：
        10            00001010
          3            00000011 
         9            00001001
         12            00001100 
再将以上二进制拼接起来计算十进制结果：00001010 00000011 00001001 00001100 = ？

import sys


def e1_10(ip):
    split_ips = ip.split(".")
    print(split_ips)

    r = ""

    for split_ip in split_ips:
        s_temp = str(bin(int(split_ip)))[2:]
        while len(s_temp)<8:
            s_temp = "0" + s_temp
        r = r + s_temp

    print("r:", r)


    print(int(r,2))


e1_10("10.3.9.12")

print(sys.getrecursionlimit())
'''

class Stack:
    def __init__(self):
        self.stack = []

    def push(self, element):
        self.stack.append(element)

    def pop(self):
        if len(self.stack) > 0:
            return self.stack.pop()
        else:
            print("stack is empty")

    def get_top(self):
        if len(self.stack) > 0:
            return self.stack[-1]
        else:
            return None


myStack = Stack

myStack.push(1)
myStack.push(2)
myStack.push(3)
myStack.push(4)


print(myStack)


