# -*- coding: UTF-8 -*-

import numpy as np
import pandas as pd
import tempfile
import os.path
def test_csv_save():
    np.random.seed(40)
    a = np.random.rand(1, 4)
    a[0][2] = np.nan
    print(a)
    np.savetxt('d:/temp/np.csv',a,fmt='%.0f',delimiter=',',header="#1,#2,#3,#4")
    df = pd.DataFrame(a)
    df.to_csv('d:/temp/pd.csv', float_format='%.0f', na_rep="NAN!")

def npy_dataFram():
    np.random.seed(42)

    a = np.random.rand(365,4)

    tmpf = tempfile.NamedTemporaryFile()
    np.savetxt(tmpf,a,delimiter=',')
    print("Size CSV file",os.path.getsize(tmpf.name))


    tmpf = tempfile.NamedTemporaryFile()
    np.save(tmpf,a)
    tmpf.seek(0)
    loaded = np.load(tmpf)
    print("Shape",loaded.shape)
    print("Size .npy file",os.path.getsize(tmpf.name))

    df = pd.DataFrame(a)
    df.to_pickle(tmpf.name)
    print("Size pickled dataframe",os.path.getsize(tmpf.name))
    print("DF from pickle\n",pd.read_pickle(tmpf.name))



if __name__ == '__main__':
    test_csv_save()
    #npy_dataFram()

