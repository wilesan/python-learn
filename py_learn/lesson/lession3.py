# -*- coding:utf8 -*-

import matplotlib.pyplot as plt
import statsmodels.api as sm
import pandas


def moving_average():
    data_loader = sm.datasets.sunspots.load_pandas()
    df = data_loader.data
    year_range = df["YEAR"].values
    print("sunactivity mean:",df["SUNACTIVITY"].mean())
    plt.plot(year_range,df["SUNACTIVITY"].values,label="Original")
    plt.plot(year_range,pandas.Series.rolling(df,11).mean()["SUNACTIVITY"].values,label="SMA11")
    plt.plot(year_range,pandas.Series.rolling(df,25).mean()["SUNACTIVITY"].values,label="SMA22")
    plt.legend()
    plt.show()

if __name__ == "__main__":
    moving_average()