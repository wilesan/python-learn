'''-*-utf8-*-'''
import numpy as np
import pkgutil as pu
import matplotlib as mpl
import scipy as sp
import pydoc
import scipy.misc as misc
import matplotlib.pyplot as plt


def pythonsum(n):
    a = range(n)
    b = range(n)
    c = []
    for i in range(n):
        a[i] = i ** 2
        b[i] = i ** 3
        c.append(a[i] + b[i])
    return c

def numpysum(n):
    a = numpy.arrange(n) ** 2
    b = numpy.arrange(n) ** 3
    c = a + b
    return c

def lena():
    lena = sci_mis.ascent()
    acopy = lena.copy()
    aview = lena.view()
    plt.subplot(221)
    plt.imshow(lena)
    plt.subplot(222)
    plt.imshow(acopy)
    plt.subplot(223)
    plt.imshow(aview)
    aview.flat = 0
    plt.subplot(224)
    plt.imshow(aview)
    plt.show()


def fancy():
   ascent = misc.ascent()
   xmax = ascent.shape[0]
   ymax = ascent.shape[1]
   ascent[range(xmax),range(ymax)] = 0
   ascent[range(xmax-1,-1,-1),range(ymax)] = 0
   plt.imshow(ascent)
   plt.show()

print("Numpy version",np.__version__)
print("Scipy version",sp.__version__)
print("Matplotlib version",mpl.__version__)

def clean(astr):
    s = astr
    s = ' '.join(s.split())
    s = s.replace('=','')
    return s

def print_desc(prefix,pkg_path):
    for pkg in pu.iter_modules(path=pkg_path):
        name = prefix + "." + pkg[1]
        if pkg[2] == True:
            try:
                docstr = pydoc.plain(pydoc.render_doc(name))
                docstr = clean(docstr)
                start = docstr.find("DESCRIPTION")
                docstr = docstr[start:start+140]
                print(name,docstr)
            except:
                continue



if __name__ == "__main__":
    #pythonsum(20)
    #numpysum(3000)
#    lena()
    #fancy()
    print_desc("numpy",np.__path__)

    print_desc("scipy",sp.__path__)
