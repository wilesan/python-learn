from bs4 import BeautifulSoup
from nltk.corpus import stopwords
import urllib.request
import nltk




response = urllib.request.urlopen('http://php.net')
html = response.read()
print(html)
print("=====================================================================================")
soup = BeautifulSoup(html,'html5lib')

text = soup.get_text(strip=True)
print(text)
print("=====================================================================================")
tokens = text.split()
print(tokens)
sr = stopwords.words('english')
'''sr = {'the','of','release','in','can','be','found','you','to','this','a','test','PHP','for','is','and'}'''
clean_tockens = list()

for token in tokens:
    if token not in sr:
        clean_tockens.append(token)

freq = nltk.FreqDist(clean_tockens)
for key,val in freq.items():
    print(str(key) + ":" + str(val))

freq.plot(20,cumulative = False)