import functools
from functools import reduce



t=('a','b',['A','B'])


print(t)

def person(name,age,**kw):
    if 'city' in kw:
        pass
    if 'job' in kw:
        pass
    print('name:', name , 'age:' , age , 'other:',kw)



extra={'city': 'Beijing','job': 'Engineer'}

person('Jack', 24, city=extra['city'], job=extra['job'])

person('ss', 33, **extra)



# map/reduce
def f(x):
    return x*x
r = map(f,[1,2,3,4,5,6,7,8,9])
a = list(r)
print(a)

print(list(map(str, [1,2,3,4,5,6,7,8,9])))

def add(x,y):
    return x+y
print(reduce(add,[1,3,5,7,9]))


def list_upper(s):
    return str.lower(s).capitalize()

print(list(map(list_upper,['adam','LISA','barT'])))



# filter
def is_odd(n):
    return n % 2 == 1

print(list(filter(is_odd,[1,2,4,5,6,7,8])))

print(sorted(['bob', 'about', 'Zoo', 'Credit'], key=str.lower, reverse=True))

def createCounter(k):
    c = k;
    def counter():
        return 1
    c = c + counter()
    return c

for i in range(5):
    print(createCounter(i))

print(list(map(lambda x:x*x,[1,2,4,6,75])))

def log(func):
    @functools.wraps(func)
    def wrapper(*agrs, **kw):
        print('call %s()', func.__name__)
        return func(*agrs,**kw)
    return wrapper()

@log
def now():
    print("2019-03-03")

int2 = functools.partial(int,base=2)

print(int2('101010'))