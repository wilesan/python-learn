import scrapy

class ClSpider(scrapy.Spider):
    name = "cls"
    start_urls = [
        "http://cl.dc72.xyz/thread0806.php?fid=7"
    ]

    def parse(self, response):
        for cl in response.xpath('//tr[@class="tr3 t_one tac"]/td[@class="tal"]/h3/a'):
            t_text = cl.css('font::text').get()
            if t_text == "" or t_text is None:
                t_text = cl.css('::text').get()
            yield {
                'title': t_text,
                'url': response.urljoin(cl.attrib['href'])
            }

        next_page = response.xpath('//a[contains(.,"下一")]')[0].attrib['href']
        if next_page is not None:
            yield response.follow(next_page,callback=self.parse)
