import matplotlib.pyplot as plt
import pandas as pd


with open('C:\\Users\\SS\\Desktop\\temp\\20200424.json', 'r', encoding='UTF-8') as file:
         df = pd.read_json(file.read())

#绘制小姐姐区域分布柱状图,x轴为地区，y轴为该区域的小姐姐数量
plt.rcParams['font.sans-serif']=['SimHei'] #用来正常显示中文标签

r1,_ = df.loc[df.weight <= '45kg'].shape
r2,_ = df.loc[(df.weight > '45kg') & (df.weight <= '50kg')].shape
r3,_ = df.loc[(df.weight > '50kg') & (df.weight <= '55kg')].shape
r4,_ = df.loc[df.weight > '55kg'].shape

labels = ['<=45kg','45kg~50kg','50kg~55kg','>55kg']
count = [r1,r2,r3,r4]

plt.text(1,-1.2,'By:wilesan')
plt.axis('equal')   #该行代码使饼图长宽相等

plt.pie(count,explode=(0,0.05,0,0),labels=labels,autopct='%1.1f%%',startangle=150,shadow=True)
plt.title("体重分布图",fontsize = 24)
plt.legend(labels,loc='upper right', fontsize=10, frameon=True, fancybox=True, framealpha=0.2, borderpad=0.3,
           ncol=1, markerfirst=True, markerscale=1, numpoints=1, handlelength=3.5)
plt.show()

