import requests
import json
import re
import requests
import datetime
from bs4 import BeautifulSoup
import os

def craw_pic(link):
    headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
        }
    pic_urls = []
    try:
        response = requests.get(link, headers=headers)
        print(response.status_code)

        # 将一段文档传入BeautifulSoup的构造方法,就能得到一个文档的对象, 可以传入一段字符串
        soup = BeautifulSoup(response.text, 'lxml')

        # 返回的是class为table-view log-set-param的<table>所有标签
        #pic_store_urls = soup.find_all('div', {'class': 'summary-pic'})
        pic_store_urls = soup.select('div[class=tab_box]')[0].select('img')
        for pic_url in pic_store_urls:
            url = pic_url.get('src').replace('_250_300','')
            pic_urls.append(url)
    except Exception as e:
        print(e)

    down_pic("许佳琪", pic_urls)

def down_pic(name,pic_urls):
    '''
    根据图片链接列表pic_urls, 下载所有图片，保存在以name命名的文件夹中,
    '''
    path = 'C:\\Users\\SS\\Desktop\\temp\\pics3\\'+name+'\\'

    if not os.path.exists(path):
      os.makedirs(path)

    for i, pic_url in enumerate(pic_urls):
        try:
            pic = requests.get(pic_url, timeout=15)
            string = '1_'+str(i + 1) + '.jpg'
            with open(path+string, 'wb') as f:
                f.write(pic.content)
                print('成功下载第%s张图片: %s' % (str(i + 1), str(pic_url)))
        except Exception as e:
            print('下载第%s张图片时失败: %s' % (str(i + 1), str(pic_url)))
            print(e)
            continue


if __name__ == '__main__':
    name = '许佳琪'
    base_link = 'http://www.win4000.com/mt/xujiaqi.html';

    pic_urls = craw_pic(base_link)

