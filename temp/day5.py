import requests
import json
import re #正则匹配
import time #时间处理模块
import jieba #中文分词
import collections
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager
from PIL import Image
from wordcloud import WordCloud  #绘制词云模块
import paddlehub as hub
import nltk
import copy

matplotlib.rcParams['font.sans-serif'] = 'SimHei'
craw_file = "C:\\Users\\SS\\Desktop\\temp\\day5\\comment.txt"
craw_file_new = "C:\\Users\\SS\\Desktop\\temp\\day5\\comment_new.txt"

def craw_data():
    base_url = "https://sns-comment.iqiyi.com/v3/comment/get_comments.action?agent_type=118&agent_version=9.11.5&authcookie=adE0KVNM6GsTkJTMHodv9WaTcUDyD69q98Hm2LOqj3MJpk3F957m3m13Nm1FPcrvvW2vJa89&business_type=17&content_id=15068748500&hot_size=0&last_id=240666242621&page=&page_size=40&types=time&callback=jsonp_1588054886893_46482"

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
    }
    last_id = ''
    c = 0;
    f_craw_file = open(craw_file, 'a',encoding='utf-8')
    try:
        for i in range(0,100):
            url = base_url[:base_url.find("last_id")+8]+last_id+base_url[base_url.find("&page=&"):]
            response = requests.get(url, headers=headers)
            source_text = response.text[response.text.find("(")+1:response.text.find("}catch(e)")-2]
            source_json = json.loads(source_text)
            comments = source_json['data']['comments']
            print("爬取地址:%s" %url)
            print("爬取该地址第一条:%s" %(comments[0]['content']))

            for comment in comments:
                if 'content' not in comment:
                    continue
                f_craw_file.write(comment['content'] + '\n')
                last_id = comment['id']
                c+=1
                #print("写入文件第%d条：%s" %(c,comment['content']))
    except Exception as e:
        print(e)
        f_craw_file.close
    f_craw_file.close
    print("共爬取%d条" %c)


def clear_special_char(content):
    '''
    正则处理特殊字符
    参数 content:原文本
    return: 清除后的文本
    '''
    f_clear = open(craw_file_new,'a',encoding='utf-8')
    clear_content = re.findall('[\u4e00-\u9fa5a-zA-Z0-9]+',content,re.S)   #只要字符串中的中文，字母，数字
    str=','.join(clear_content)
    f_clear.write(str+'\n')
    f_clear.close
    return str


if __name__ == '__main__':
    #爬取数据
    #craw_data()
    #读取数据
    #file = open(craw_file, mode='r',encoding='utf-8')
    #s = file.read()
    #clear_special_char(s)
    #分词
    '''
    stopwords = [line.strip() for line in open("C:\\Users\\SS\\Desktop\\EastMa-stopwords-master\\stopwords\\百度停用词表.txt", encoding='UTF-8').readlines()]
    jieba.load_userdict("C:\\Users\\SS\\Desktop\\my_dic.txt")
    file = open(craw_file_new, mode='r',encoding='utf-8')
    s = file.read().replace(',','')
    seg = []
    cut_word = jieba.lcut(s)
    freq = nltk.FreqDist(cut_word)
    new_freq = copy.deepcopy(freq)
    for key,value in freq.items():
        if key in stopwords:
            new_freq.__delitem__(key)
    f10 = new_freq.most_common(10)
    name_list = []
    count_list = []
    for k,v in f10:
        name_list.append(k)
        count_list.append(v)
    plt.bar(name_list,count_list)
    plt.show()
    '''
    stopwords = [line.strip() for line in open('./stopwords.txt', encoding='UTF-8').readlines()]
    jieba.load_userdict('./my_dic.txt')
    file = open(craw_file_clear, mode='r', encoding='utf-8')
    s = file.read()
    text = ' '.join(jieba.lcut(s))
    mask = np.array(Image.open(r'humanseg_output/1_14.png'))
    wc = WordCloud(mask=mask, font_path=r'fonts/SimHei.ttf', mode='RGBA', background_color=None).generate(text)

    # 从图片中生成颜色
    image_colors = ImageColorGenerator(mask)
    wc.recolor(color_func=image_colors)

    # 显示词云
    plt.imshow(wc, interpolation='bilinear')
    plt.axis("off")
    plt.show()

    # 保存到文件
    wc.to_file('mycloud.png')



