import numpy as np
from pandas.io.parsers import  read_csv
df = read_csv("C:/Users/SS/Desktop/WHO.csv")
#内容
print("内容:Dataframe",df)
#形状
print("形状:Shape",df.shape)
#行数
print("行数:length",len(df))
#列名
print("列名:Column",df.columns)
#每列数据类型
print("每列数据类型:Data type",df.dtypes)
#索引
print("索引:Index",df.index)
#空值显示为nan
print("Values",df.values)


###Series例子
country_col = df["Country"]
print("Type df",type(df))
print("Type country col",type(country_col))
print("Series shape",country_col.shape)
print("Series index",country_col.index)
print("Series values",country_col.values)
print("Series name",country_col.name)
print("Last 2 countries", country_col[-2:])
print("Last 2 countries type", type(country_col[-2:]) )
#用numpy的sign来获取数字的符号：正数返回1，负数-1，零0
#print("df signs", np.sign(df))
last_col = df.columns[-1]
print("Last df column signs", last_col)
print("Last sign",np.sign(df[last_col]))
