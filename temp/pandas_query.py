import numpy as np
from pandas.io.parsers import  read_csv
sunspots = read_csv("C:/Users/SS/Desktop/SIDC-SUNSPOTS_A.csv")
print("sunspots",sunspots)
print("Head 2",sunspots.head(2))
print("Tail 2",sunspots.tail(2))

last_date = sunspots.index[0]
print("Last values",sunspots.loc[last_date])
#print("Values slice by date",sunspots["20020101":"20131231"])
#用索引查询
print("Slice from a list of indices",sunspots.iloc[[2,4,-4,-2]])
print("Boolean selection",sunspots[sunspots > sunspots.mean()])
#print("Boolean selection with column label",sunspots[sunspots. Number>sunspots.Number.mean()])
print("Describe",sunspots.describe())