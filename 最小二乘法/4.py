from pylab import *

from pylab import *

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(1, 10, 100)
X = np.ones((len(x), 2))
X[:, 0] = x
W_true = np.array([2, 3])
y = X.dot(W_true)
y_noise = y + np.random.randn(len(x))
fig = plt.figure(figsize=(8, 6))
plt.scatter(x, y_noise, s=10, c='r', marker='o')
plt.plot(x, y, label='true value', c='b')
plt.legend()
plt.show()